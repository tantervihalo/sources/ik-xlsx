import 'dotenv/config';
import Excel from 'exceljs';
import fetch from 'node-fetch';
import {TantervihaloLoader} from './api.js';
import {mkdir, writeFile} from 'node:fs/promises';
import {resolve, join} from 'node:path';
import {createHash} from 'node:crypto';

const url = process.env['XLSX_URL'];
const outDir = process.env['OUTPUT_DIR'] || 'outputs';
const outFileName = process.env['OUTPUT_FILE'] || `${outDir}.json`;
const res = await fetch(url);
const bytes = await res.arrayBuffer();
const workbook = new Excel.Workbook();
await workbook.xlsx.load(bytes);
await mkdir(outDir, {recursive: true});
const loader = new TantervihaloLoader(workbook);
let found = 0, written = 0;
const writePromises = [];
loader.addEventListener('worksheetIgnored', ({detail: {worksheet}}) => {
	log('⚠️', `Worksheet ${worksheet.name} ignored (${worksheet.state})`);
})
loader.addEventListener('tantervihaloFound', ({detail: {tantervihalo, promise}}) => {
	writePromises.push(new Promise(async (resolve) => {
		let file, title, error;
		try {
			found++;
			title = tantervihalo.title;
			log('>', title);
			await promise;
			log('<', title, {
				written: written + 1,
			});
			const json = JSON.stringify(tantervihalo);
			file = join(outDir, `${title}.json`);
			await writeFile(file, json, 'utf-8');
			written++;
			log('✔️', title);
		} catch (e) {
			written++;
			log('❌', title);
			console.log(e);
			error = e;
		} finally {
			resolve({
				title,
				file,
				error,
			});
		}
	}));
});
loader.loadedPromise.then(async () => {
	log('✅', "Discovered every sheet", {
		written: written.toString().replace(/./g, ' '),
	});
	const json = JSON.stringify({
		source: {
			url,
			headers: [...res.headers.entries()].reduce((a, [k, v]) => ({...a, [k]: v}), {}),
			hash: {
				algorithm: 'sha256',
				encoding: 'hex',
				digest: createHash('sha256')
					.update(Buffer.from(bytes))
					.digest('hex'),
			},
		},
		created: {
			at: workbook.created,
			by: workbook.creator,
		},
		lastModified: {
			at: workbook.modified,
			by: workbook.lastModifiedBy,
		},
		jsons: await Promise.all(writePromises),
	});
	const file = resolve(outDir, outFileName);
	await writeFile(file, json, 'utf-8');
	console.log(`Output written to ${file}`);
}, e => {
	log('⛔', "Fatal error occurred");
	console.log(e);
})

function log(key, msg, overrides = {}) {
	const {written: _written = written, found: _found = found} = overrides;
	console.log(`[${_written}/${_found}] ${key} ${msg}`);
}


